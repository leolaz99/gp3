// Fill out your copyright notice in the Description page of Project Settings.

#include "GP3Character.h"
#include "GameFramework/Character.h"

// Sets default values
AGP3Character::AGP3Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("Spring Arm");
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
}

// Called when the game starts or when spawned
void AGP3Character::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called to bind functionality to input
void AGP3Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


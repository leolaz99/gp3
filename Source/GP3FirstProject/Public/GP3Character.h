// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "GP3Character.generated.h"

UCLASS()
class GP3FIRSTPROJECT_API AGP3Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGP3Character();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = GPC)
		USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, Category = GPC)
		UCameraComponent* Camera;

public:	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
